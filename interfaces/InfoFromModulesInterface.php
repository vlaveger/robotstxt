<?php

namespace app\components\robotstxt\interfaces;

/**
 * Interface InfoFromModulesInterface
 */
interface InfoFromModulesInterface
{

    /**
     * @param array $modules
     * @return array
     */
    public function pickUpCrawlerInfoFromModules(array $modules): array;
}