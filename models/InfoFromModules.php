<?php

namespace app\components\robotstxt\models;

use app\components\robotstxt\interfaces\InfoFromModulesInterface;
use yii\helpers\ArrayHelper;

class InfoFromModules implements InfoFromModulesInterface
{
    /**
     * Ищет данные для robot.txt каждого модуля 
     * @param array $modules
     * @return array
     */
    public function pickUpCrawlerInfoFromModules(array $modules): array {
        $robotsEntryInModules = [];
        
        //Поиск данных для robot.txt в конфигурации модуля
        foreach ($modules as $moduleName => $moduleParams) {
            if (is_array($moduleParams)) {
                if (ArrayHelper::keyExists('robotsTxt', $moduleParams)) {
                    $robotsEntryInModules = ArrayHelper::merge($robotsEntryInModules, $this->addEntryFromModule($moduleParams['robotsTxt'], $moduleName)); 
                }
            }        
            //Запрос данных для robots.txt у самого модуля
            $module = \Yii::$app->getModule($moduleName);
            if (method_exists($module, 'getDataForRobotsTxt')){
               $robotsEntryInModules = ArrayHelper::merge($robotsEntryInModules, $module->getDataForRobotsTxt());
               
            }
        }
        return $robotsEntryInModules;
    }   

    /**
     * Формирует массив запрещающих правил для robots.txt из конфигурации модуля
     * @param array $moduleParams
     * @param string $moduleName
     * @return array
     */
    private function addEntryFromModule(array $moduleParams, string $moduleName) {
        $robotsEntryInModule = [];
            foreach ($moduleParams as $crawler => $params) {
                if (ArrayHelper::keyExists('disallow', $params)) {
                    //К параметрам, указанным в конфигурации модуля, добавляется имя модуля
                    $params = $this->addModuleNameForArray($params, $moduleName);
                    
                    $robotsEntryInModule[$crawler]['disallow'] = 
                            empty($robotsEntryInModule[$crawler]['disallow']) ? 
                                $params['disallow'] : ArrayHelper::merge($robotsEntryInModule[$crawler]['disallow'], $params['disallow']);
                }
            }
        return $robotsEntryInModule;
    }
    
    /**
     * К каждому правилу, описанному в конфигурации модуля добавляет имя модуля.
     * @param array $arr
     * @param string $moduleName
     * @return array
     */
    private function addModuleNameForArray(array $arr, string $moduleName) {
        $result = [];
        foreach ($arr as $key => $value) {
            $result[$key] = substr_replace($value, '/' . $moduleName . '/', 0, 1);
        }
        return $result;
    }
}
