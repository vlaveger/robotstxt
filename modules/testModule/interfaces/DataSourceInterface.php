<?php

namespace app\modules\testModule\interfaces;

/**
 * Interface InfoFromModulesInterface
 */
interface DataSourceInterface
{

    /**
     * @return array
     */
    public function getData(): array;
}