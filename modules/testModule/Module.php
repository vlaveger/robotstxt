<?php

namespace app\modules\testModule;

use app\modules\testModule\interfaces\DataSourceInterface;

class Module extends \yii\base\Module
{
    public $robotsTxt;
    
    private $_dataSource;

    public function __construct($id, $parent = null, DataSourceInterface $dataSource, $config = []) {
        parent::__construct($id, $parent, $config);
        $this->_dataSource = $dataSource;
    }
    
    /**
     * Получает данные из источника данных и формирует массив для robotTxt
     * @return array
     */
    public function getDataForRobotsTxt() {
        $result = [];
        $posts = $this->_dataSource->getData();
        
        foreach ($posts as $post) {
            foreach ($post['robots'] as $robot => $enabled) {
                if ($enabled) {
                    $result[$robot]['allow'][] = $post['url'];
                }
                else {
                    $result[$robot]['disallow'][] = $post['url'];
                }
            }
        }
        return $result;
    }    
}

