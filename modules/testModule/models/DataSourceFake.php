<?php

namespace app\modules\testModule\models;

use app\modules\testModule\interfaces\DataSourceInterface;

/**
 * Класс заглушка, выполняющий роль источника данных
 */
class DataSourceFake implements DataSourceInterface
{
    private $_posts = [
            'post1' => ['url' => '/post1', 'robots' => ['Yandex' => true, 'Google' => false]],
            'post2' => ['url' => '/post2', 'robots' => ['*' => true]],
            'post3' => ['url' => '/post3', 'robots' => ['*' => false, 'Google' => true]],
            'post4' => ['url' => '/post4', 'robots' => ['Yandex' => true]],
            'post5' => ['url' => '/post5', 'robots' => ['Yandex' => false]],
    ];    
    
    public function getData(): array {
        return $this->_posts;
    }
}