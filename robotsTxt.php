<?php

namespace app\components\robotstxt;

use Yii;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use app\components\robotstxt\interfaces\InfoFromModulesInterface;

class RobotsTxt extends BaseObject
{  
    public $blockAllSite;
    public $crawlers = [];
    public $sitemap = '';   
    public $cleanParam = [];   
    public $crawlDelay = '';   
    
    const ALL = '/';
    const CR = "\n";

    
    /**
     * На основе конфигурации компонента и конфигурации модулей приложения
     * формируется массив с правилами для robots.txt
     * @return array
     */
    public function createContent()
    {
        $arrayWithUrl = [];
                
        //Блокирует доступ ко всему ресурсу полностью, если это задано в конфигурации
        if ($this->blockAllSite) {
            return $arrayWithUrl = array("User-agent: *" . $this::CR, "Disallow" . ": " . $this::ALL);
        }
        
        //Создаются отдельные блоки под каждого паука, перечисленного в конфигурации
        if ($this->crawlers){
            foreach ($this->crawlers as $crawler => $params) {
                $arrayWithUrl[$crawler] = $params;
            }
        }
        $infoFromModules = \YII::$container->get('infoFromModules');
        
        //Сбор информации из модулей
        $arrayWithUrl = ArrayHelper::merge($arrayWithUrl, $infoFromModules->pickUpCrawlerInfoFromModules(\Yii::$app->modules));
        
        // Формирование итогового массива для записи robots.txt на основе полученной информации
        // Каждый элемент массива - будущая строка в файле robots.txt   
        return $this->ArrayWithURLToArrayWithRules($arrayWithUrl);
        
    }
    
    /**
     * Сохраняет полученный ранее массив в файл robots.txt
     * @param array $arrayWithUrl
     */
    public function saveFile(array $arrayWithUrl)
    {
        $path = Yii::getAlias('@webroot').'/robots.txt';
        file_put_contents(Yii::getAlias('@webroot').'/robots.txt', $arrayWithUrl);
    }
    
    /**
     *
     * @param array $arrayWithUrl
     * @return array
     */
    public function ArrayWithURLToArrayWithRules(array $arrayWithUrl): array {
        $result = [];                
        foreach ($arrayWithUrl as $userAgent => $rules) {
            $result[] = 'User-agent: ' . $userAgent  . $this::CR;
            if (is_array($rules)) {
                foreach ($rules as $rule => $urls) {
                    if (is_array($urls)) {
                        foreach ($urls as $url) {
                            $result[] = StringHelper::mb_ucfirst($rule) . ": " . $url . $this::CR;
                        }                        
                    }
                    elseif(is_string($urls)) {
                        if ($urls){
                            $result[] = StringHelper::mb_ucfirst($rule) . ": " . $urls . $this::CR;                            
                        }
                    }
                }
            }
            $result [] = $this::CR;
        }
        return $result;        
    }
}