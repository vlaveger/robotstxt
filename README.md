В файле app\config\common.php прописать:

    'components' => [
        ...
        'robotsTxt' => [
            'class' => app\components\robotstxt\robotsTxt::class,
            'blockAllSite' => 0,
            'crawlers' => [
                '*' => [
                    'allow' => ['/', '/css'],
                    'disallow' => ['/photo/tags', '/photo/search'],
                    'sitemap' => '',
                    'cleanParam' => 'utm_source&utm_medium&utm_term&utm_campaign /',
                    'crawlDelay' => '2',
                ]
            ] 
        ...

    'modules' => [
        ...
        'testModule' => [
            'class' => 'app\modules\testModule\Module',
            'robotsTxt' => [
                '*' => [
                    'disallow' => ['/', '/login'],
                ],                
                'Yandex' => [
                    'disallow' => ['/log', '/css', '/js'],
                ],                
            ]
        ],    
        ...

    'definitions' => [
        ...
            'infoFromModules' => [
                'class' => 'app\components\robotstxt\models\InfoFromModules',
            ],    
            'app\modules\testModule\interfaces\DataSourceInterface' => [
                'class' => 'app\modules\testModule\models\DataSourceFake',
            ],
        ...